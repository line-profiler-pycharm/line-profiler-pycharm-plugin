package nl.jusx.pycharm.lineprofiler.service

import com.intellij.openapi.application.EDT
import com.intellij.openapi.components.Service
import com.intellij.openapi.diagnostic.logger
import com.intellij.openapi.project.Project
import com.intellij.openapi.projectRoots.Sdk
import com.jetbrains.python.packaging.PyPackageInstallUtils
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.jetbrains.concurrency.Promise
import org.jetbrains.concurrency.asPromise

private val LOG = logger<LineProfilerPipService>()

@Service
class LineProfilerPipService(
        private val cs: CoroutineScope
) {

    /**
     * This method is an altered version of the PyPackageInstallUtils.confirmAndInstall version
     * It is changed so that an upgrade can be triggered of a package
     */
    suspend fun confirmAndUpgrade(project: Project, sdk: Sdk, packageName: String) {
        val isConfirmed = withContext(Dispatchers.EDT) {
            PyPackageInstallUtils.confirmInstall(project, packageName)
        }
        if (!isConfirmed)
            return
        PyPackageInstallUtils.upgradePackage(project, sdk, packageName)
    }


    suspend fun launchEnsureLineProfilerPycharmPackageInstalled(project: Project, sdk: Sdk) {
        // Check whether lineprofiler is installed
        val packageVersion = PyPackageInstallUtils.getPackageVersion(project, sdk, "line-profiler-pycharm")
        if (packageVersion == null || packageVersion.lessThan(1, 2)) {
            // Not installed
            // Install line-profiler-pycharm
            confirmAndUpgrade(project, sdk, "line-profiler-pycharm")
        }
    }


    fun ensureLineProfilerPycharmPackageInstalled(project: Project, sdk: Sdk?): Promise<*> {
        return cs.launch(Dispatchers.EDT) {
            if (sdk != null) {
                launchEnsureLineProfilerPycharmPackageInstalled(project, sdk)
            }
        }.asPromise().onError {
            LOG.error("Could not install line-profiler-pycharm, try manually", it)
        }
    }
}