package nl.jusx.pycharm.lineprofiler.executor;

import com.intellij.execution.configurations.RunConfiguration;
import com.intellij.execution.configurations.RunProfile;
import com.intellij.execution.configurations.RunProfileState;
import com.intellij.execution.process.ProcessHandler;
import com.intellij.execution.runners.ExecutionEnvironment;
import com.intellij.execution.ui.RunContentDescriptor;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.projectRoots.Sdk;
import com.intellij.openapi.util.io.FileUtil;
import com.jetbrains.python.run.AbstractPythonRunConfiguration;
import com.jetbrains.python.run.PythonCommandLineState;
import com.jetbrains.python.run.PythonRunConfiguration;
import com.jetbrains.python.run.PythonRunner;
import com.jetbrains.python.testing.PyAbstractTestConfiguration;
import nl.jusx.pycharm.lineprofiler.profile.Profile;
import nl.jusx.pycharm.lineprofiler.render.ProfileHighlightService;
import nl.jusx.pycharm.lineprofiler.service.LineProfilerPipService;
import nl.jusx.pycharm.lineprofiler.service.TimeFractionCalculation;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.concurrency.Promise;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;


public class ProfileRunner extends PythonRunner {
    private static final Logger logger = Logger.getInstance(ProfileRunner.class.getName());

    @NotNull
    @Override
    public String getRunnerId() {
        return ProfileExecutor.PROFILE_EXECUTOR_ID;
    }

    @Override
    public boolean canRun(@NotNull String executorId, @NotNull RunProfile profile) {
        return executorId.equals(ProfileExecutor.PROFILE_EXECUTOR_ID) && profile instanceof AbstractPythonRunConfiguration;
    }

    /**
     * Starts the Profile Lines execution
     * <p>
     * If a .pclprof file is written during execution it is visualized after completion
     * <p>
     * Location of the .pclprof file is defined with the `line-profiler-pycharm` environment
     * variable:
     * PC_LINE_PROFILER_STATS_FILENAME
     */
    @Override
    protected @NotNull Promise<RunContentDescriptor> execute(@NotNull ExecutionEnvironment env, @NotNull RunProfileState state) {
        ProfileHighlightService profileHighlightService = env.getProject().getService(ProfileHighlightService.class);
        ApplicationManager.getApplication().invokeLater(profileHighlightService::disposeAllVisualizations);

        PythonCommandLineState pcl_state = (PythonCommandLineState) state;

        // It's also possible to get the run_config from the ExecutionEnvironment, however this is the runconfiguration
        // may not be the one that is used. In the case of unittests, the runconfig is copied from environment and
        // stored in state. Any changes to the run configuration in the ExecutionEnvironment are then not picked up
        AbstractPythonRunConfiguration<?> runConfiguration = pcl_state.getConfig();

        // Define the location where the .pclprof file must be saved by our `line-profiler-pycharm` python package
        Path pclprofPath;
        String pclprofFilename;
        if (runConfiguration.getWorkingDirectory() == null || runConfiguration.getWorkingDirectory().equals("")) {
            // Create temporary file to which a .pclprof will be written.
            try {
                pclprofPath = Files.createTempFile("line_profile", ".pclprof");
                // Use absolute path as file name for temporary file
                pclprofFilename = pclprofPath.toString();
                pclprofFilename = FileUtil.getNameWithoutExtension(pclprofFilename);
            } catch (IOException e) {
                throw new RuntimeException("Could not create temporary .pclprof file for Profile Line execution");
            }
        } else {
            String pclprofFileName = getPclprofFileName(runConfiguration);

            pclprofFilename = Paths.get(pclprofFileName).getFileName().toString();
            pclprofPath = Paths.get(runConfiguration.getWorkingDirectory(), pclprofFilename + ".pclprof");
        }

        // Remove file if it already exists
        try {
            Files.deleteIfExists(pclprofPath);
        } catch (IOException e) {
            throw new RuntimeException("Could not remove already existing " + pclprofPath + " for Profile Line execution");
        }

        // Set the environment variable for `line-profile-pycharm`
        runConfiguration.getEnvs().put("PC_LINE_PROFILER_STATS_FILENAME", pclprofFilename);

        // Ensure line-profiler-pycharm package installation to python env
        Sdk sdk = runConfiguration.getSdk();
        LineProfilerPipService lineProfilerPipService = ApplicationManager.getApplication().getService(LineProfilerPipService.class);
        Promise<?> promisePipPackage = lineProfilerPipService.ensureLineProfilerPycharmPackageInstalled(env.getProject(), sdk);

        // Truly start the python execution
        Promise<RunContentDescriptor> promise = promisePipPackage.thenAsync(o -> super.execute(env, state));

        // After creating an execution, the environment variable can be removed again from our run config
        promise.then(runContentDescriptor -> {
            runConfiguration.getEnvs().remove("PC_LINE_PROFILER_STATS_FILENAME");
            return runContentDescriptor;
        });

        // Add listener to process completion that triggers profile visualization
        promise.onSuccess(runContentDescriptor -> ApplicationManager.getApplication().executeOnPooledThread(() -> {
            ProcessHandler ph = runContentDescriptor.getProcessHandler();
            if (ph == null) {
                logger.error("Could not get processhandler after starting Line Profile run");
                return;
            }
            if (ph.waitFor()) {
                triggerPclprofVisualization(env.getProject(), pclprofPath);
            }
        }));

        return promise;
    }

    private String getPclprofFileName(RunConfiguration runConfiguration) {
        if (runConfiguration instanceof PythonRunConfiguration) {
            // Set filename of script as filename path
            // a module script `some_module` would become `some_module`
            // a normal script `some_dir/some_script.py` would become `some_script.py`
            // .pclprof will be written to the working directory
            return ((PythonRunConfiguration) runConfiguration).getScriptName();
        } else if (runConfiguration instanceof PyAbstractTestConfiguration) {
            // Set the test target as filename
            // for example: test_main.MainTest.test_main.pclprof
            return ((PyAbstractTestConfiguration) runConfiguration).getTarget().getTarget();
        }

        return "line_profile";
    }

    /**
     * Looks for the just created .pclprof file and visualizes it
     * <p>
     * The .pclprof file should have been created by `line-profiler-pycharm` python package
     *
     * @param project     project
     * @param pclprofPath path to .pclprof file
     */
    private void triggerPclprofVisualization(Project project, Path pclprofPath) {
        if (!Files.exists(pclprofPath)) {
            // No .pclprof file has been created during profile, visualization not possible
            return;
        }

        ProfileHighlightService profileHighlightService = project.getService(ProfileHighlightService.class);

        Profile profile = Profile.fromPclprof(pclprofPath);

        ApplicationManager.getApplication().invokeLater(() -> {
            profileHighlightService.setProfile(profile);
            profileHighlightService.visualizeProfile(TimeFractionCalculation.FUNCTION_TOTAL);
        });
    }
}
