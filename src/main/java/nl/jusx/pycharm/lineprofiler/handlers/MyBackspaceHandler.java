package nl.jusx.pycharm.lineprofiler.handlers;

import com.intellij.codeInsight.editorActions.BackspaceHandlerDelegate;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiFile;
import nl.jusx.pycharm.lineprofiler.render.ProfileHighlightService;
import org.jetbrains.annotations.NotNull;


public class MyBackspaceHandler extends BackspaceHandlerDelegate {
    @Override
    public void beforeCharDeleted(char c, @NotNull PsiFile file, @NotNull Editor editor) {
        Project project = editor.getProject();
        if (project == null) {
            return;
        }
        ProfileHighlightService profileHighlightService = project.getService(ProfileHighlightService.class);
        profileHighlightService.disposeHighlightersOverlappingAtCaretPositions(editor);
    }

    @Override
    public boolean charDeleted(char c, @NotNull PsiFile file, @NotNull Editor editor) {
        return true;
    }
}
