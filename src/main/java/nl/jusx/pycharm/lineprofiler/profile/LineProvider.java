package nl.jusx.pycharm.lineprofiler.profile;

public interface LineProvider {
    int getLineNrFromZero();
}
