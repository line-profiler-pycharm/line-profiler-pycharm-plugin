package nl.jusx.pycharm.lineprofiler.profile;


import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.intellij.openapi.vfs.VirtualFile;
import org.jetbrains.annotations.Nullable;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class Profile {

    List<FunctionProfile> functionProfiles = new ArrayList<>();
    private long totalTime;

    private Profile(ProfileSchema schema, @Nullable String rootDirectory) {
        for (ProfileSchema.Function fSchema : schema.profiledFunctions) {
            FunctionProfile fn = new FunctionProfile(fSchema, rootDirectory);
            functionProfiles.add(fn);
            totalTime += fn.totalTime;
        }
    }

    public List<FunctionProfile> getProfiledFunctions() {
        return functionProfiles;
    }


    /**
     * Loads a .pclprof file into a Profile object
     * The .pclprof file must have been created with our own python helper package line-profiler-pycharm.
     * The .pclprof file contains Json data and is parsed with the gson library
     *
     * @param profileFile .pclprof file to load
     * @return Profile object from .pclprof file
     */
    @Nullable
    public static Profile fromPclprof(String profileFile, @Nullable String rootDirectory) {
        Gson gson = new Gson();
        JsonReader reader;

        try {
            reader = new JsonReader(new FileReader(profileFile));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }

        ProfileSchema data = gson.fromJson(reader, ProfileSchema.class); // contains the whole reviews list

        return new Profile(data, rootDirectory);
    }

    @Nullable
    public static Profile fromPclprof(Path profileFile) {
        return fromPclprof(profileFile.toString(), profileFile.getParent().toString());
    }

    @Nullable
    public static Profile fromPclprof(VirtualFile profileFile) {
        return fromPclprof(profileFile.getPath(), profileFile.getParent().getPath());
    }
}
